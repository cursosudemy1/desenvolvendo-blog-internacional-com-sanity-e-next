import { parseISO, format } from 'date-fns'
import ptBR from 'date-fns/locale/pt-BR'
import styles from './date.module.scss'

export interface DateProps {
  children: string
}

export default function Date({ children }: DateProps) {
  const date = parseISO(children)

  return (
    <time className={styles.component} dateTime={children}>
      {format(date, `d 'de' MMMM 'de' yyyy`, { locale: ptBR })}
    </time>
  )
}