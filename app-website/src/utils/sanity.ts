import sanityClient from '@sanity/client'
import imageUrlBuilder from '@sanity/image-url'

// Configuração para acessar os dados do sanity
const client = sanityClient({
  projectId: process.env.SANITY_PROJECT_ID,
  dataset: process.env.SANITY_DATASET,
  apiVersion: '2021-04-06',
  useCdn: process.env.NODE_ENV === 'production'
})

export interface Imagereference {
  _type: 'image',
  asset: {
    _ref: string,
    _type: 'reference'
  }
}

export interface BlogPost {
  slug: string,
  title: string,
  description: string,
  date: string,
  coverImage: Imagereference
}

export type GetBlogPosts = () => Promise<BlogPost[]>

export const getBlogPosts: GetBlogPosts = async () => {
  const blogPosts = await client.fetch(`
  *[_type == 'post']{
    'slug': slug.current,
    title,
    description,
    date,
    coverImage
  }
  `)
  return blogPosts
}

const builder = imageUrlBuilder(client)

// Essa função extrai a url da imagem
export function urlFor(src: Imagereference) {
  return builder.image(src)
}