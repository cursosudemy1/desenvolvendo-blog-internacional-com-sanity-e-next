# Desenvolvendo Blog Internacional com Sanity e Next

 Ferramentas e Bibliotecas 

* ReactJS 
* NextJS
* Sanity( CMS )

> Configurações 

Criar uma conta  o sanity.io

* [Sanity ( CMS )](https://www.sanity.io/)
* [Documentação do Sanity](https://www.sanity.io/docs/configuration)


Criar o arquivo .env.local ( na raiz do projeto )

```javascript
SANITY_PROJECT_ID=7f7n6o24
SANITY_API_VERSION=2021-08-14
SANITY_DATASET=production
```

<h2>AppCMS ( Gerenciador de Conteúdo )</h2>

<h3>Utilizaçao</h3>

```bash
# Acesse a pasta 
$ cd appcms

# Execute o comando para iniciar a aplicação 
$ sanity start
```

<h2>App WebSite ( Visualizador do Conteúdo )</h2>

<h3>Utilização</h3>

```bash
# Acesse a pasta 
$ cd app-website

# Execute o comando para iniciar a aplicação 
$ yarn start
```
