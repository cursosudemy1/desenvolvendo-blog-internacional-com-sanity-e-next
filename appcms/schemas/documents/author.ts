export default {
  name: 'author',
  title: 'Autor',
  type: 'document',
  fields: [
    {
      name: 'name',
      title: 'Nome',
      type: 'string'
    },
    {
      name: 'avatar',
      title: 'Avatar',
      type: 'image'
    }
  ]
}